import * as ActionType from '../ActionsTypes';

const initialState = {
    userData: null,
    isInProgress: false,
    message: null,
    userDetailsById: null,
    updateDetails:null
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.GET_USER_DETTAILS_REQUEST:
            return {
                ...state,
                isInProgress: true
            }
        case ActionType.GET_USER_DETTAILS_SUCCESS:
            return {
                ...state,
                userData: action.payload,
                isInProgress: false
            }
        case ActionType.GET_USER_DETTAILS_FAILURE:
            return {
                ...state,
                isInProgress: false,
                message: action.error
            }

        // get details by id

        case ActionType.GET_USER_DETTAILS_BY_ID_REQUEST:
            return {
                ...state,
                isInProgress: true
            }
        case ActionType.GET_USER_DETTAILS_BY_ID_SUCCESS:
            return {
                ...state,
                userDetailsById: action.payload,
                isInProgress: false
            }
        case ActionType.GET_USER_DETTAILS_BY_ID_FAILURE:
            return {
                ...state,
                isInProgress: false,
                message: action.error
            }

        // update
        case ActionType.UPDATE_USER_DETTAILS_BY_ID_REQUEST:
            return {
                ...state,
                isInProgress: true
            }
        case ActionType.UPDATE_USER_DETTAILS_BY_ID_SUCCESS:
            return {
                ...state,
                userDetailsById: action.payload,
                isInProgress: false,
            }
        case ActionType.UPDATE_USER_DETTAILS_BY_ID_FAILURE:
            return {
                ...state,
                isInProgress: false,
                message: action.error
            }

        default:
            return state;
    }
}

export default userReducer;