import React from 'react';

function UserDetails({userInfo, updateUserInfo, handelChange}) {
    console.log(userInfo);
    return(
        <div className="userInfo">
            <div className="row pt-5">
                <div className="col-sm-12 col-md-4 mx-auto card p-3">
                <form >
                    <div className="form-group">
                        <label >Email:</label>
                        <input type="email" className="form-control" value={userInfo.email} onChange={ handelChange }  name="email"/>
                    </div>
                    <div className="form-group">
                        <label >Company name:</label>
                        <input type="text" className="form-control" name="name" value={userInfo.company.name} onChange={ handelChange } />
                    </div>
                    <a href="#" className="btn btn-primary" onClick={updateUserInfo}> Submit</a>
                </form>
                </div>
            </div>
            
        </div>
        
    ) 
}

export default UserDetails;