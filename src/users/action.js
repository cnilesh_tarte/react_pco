import axios from 'axios';
import * as ActionTypes from '../ActionsTypes';
import { ToastsStore } from 'react-toasts';

// GET uses types
export const getUsers = () => dispatch => {
    dispatch(requestGetUsers());
    axios.get('https://jsonplaceholder.typicode.com/users').then(response => {
        dispatch(getUsersSuccess(response.data));
    }).catch(error => {
        dispatch(getUsersFailure(error.response))
    })
}


export function requestGetUsers() {
    return {
        type: ActionTypes.GET_USER_DETTAILS_REQUEST
    }
}

export function getUsersSuccess(data) {
    return {
        type: ActionTypes.GET_USER_DETTAILS_SUCCESS,
        payload: data
    }
}

export function getUsersFailure(error) {
    return {
        type: ActionTypes.GET_USER_DETTAILS_FAILURE,
        error
    }
}

// GET details 
export const getUserDetailsById = (id) => dispatch => {
    dispatch(requestGetUserDetailsById());
    axios.get('https://jsonplaceholder.typicode.com/users/'+ id).then(response => {
        dispatch(getUsersDetailsByIdSuccess(response.data));
    }).catch(error => {
        dispatch(getUsersDetailsByIdFailure(error.response))
    })
}

export function requestGetUserDetailsById() {
    return {
        type: ActionTypes.GET_USER_DETTAILS_BY_ID_REQUEST
    }
}

export function getUsersDetailsByIdSuccess(data) {
    return {
        type: ActionTypes.GET_USER_DETTAILS_BY_ID_SUCCESS,
        payload: data
    }
}

export function getUsersDetailsByIdFailure(error) {
    return {
        type: ActionTypes.GET_USER_DETTAILS_BY_ID_FAILURE,
        error
    }
}

// update details 
export const updateUserInfo = (info) => dispatch => {
    dispatch(updateUserInfoById());
    if(info && info.id){
        axios.put('https://jsonplaceholder.typicode.com/users/'+info.id, info).then(response => {
            alert('Updated Successfully..');
            dispatch(updateUserInfoByIdSuccess(response.data));
        }).catch(error => {
            dispatch(updateUserInfoByIdFailure(error.response))
        })
    }
   
}

export function updateUserInfoById() {
    return {
        type: ActionTypes.UPDATE_USER_DETTAILS_BY_ID_REQUEST
    }
}

export function updateUserInfoByIdSuccess(data) {
    return {
        type: ActionTypes.UPDATE_USER_DETTAILS_BY_ID_SUCCESS,
        payload: data
    }
}

export function updateUserInfoByIdFailure(error) {
    return {
        type: ActionTypes.UPDATE_USER_DETTAILS_BY_ID_FAILURE,
        error
    }
}