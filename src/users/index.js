import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUsers, getUserDetailsById, updateUserInfo} from './action';
import UserDetails  from './userDetails';

class Users extends Component {
 
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            id: null,
            userInfo:null
        }
        this.getUserData = this.getUserData.bind(this);
    }

    componentDidMount(){
        this.props.getUsers();
    }

    UNSAFE_componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.userInfo && nextProps.userInfo != null){
            this.setState( { 
                userInfo : nextProps.userInfo
            })
        }
    }

    getUserData = (event) => {
        this.props.getUserDetailsById(event.target.value);
    }

    updateUserInfo = () => {
        this.props.updateUserInfo(this.state.userInfo);
    } 

    handelChange = ( event ) =>{
        let name = event.target.name;
        let value = event.target.value;

        let userInfo = {
            ...this.state.userInfo
        }
 
        userInfo[name] = value;
        userInfo.company[name] = value

        this.setState( { 
            userInfo 
        })
    }

    render(){
        return(
        <div className="user container-fluid">
            <div className="pt-5 ">
                <div className="text-center pb-3 ">
                    <strong>Please Select User</strong>
                </div>
                <select onChange={this.getUserData} id="cars" >
                    { this.props.userData && Array.isArray(this.props.userData) && this.props.userData.map( ( res, index ) => {
                        return <option  key={index} value={res.id}>{res.name}</option>
                    } ) }
                </select>
            </div>
            
            {
                this.state.userInfo == null ? null :  <UserDetails userInfo={this.state.userInfo} handelChange={this.handelChange} updateUserInfo={this.updateUserInfo} />
            }
           
        </div>
        )
    }
} 

const mapStateToProps = (state) => {
    return {
        userData : state.userReducer.userData,
        userInfo: state.userReducer.userDetailsById
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return bindActionCreators(
        {
            getUsers, getUserDetailsById, updateUserInfo
        }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);