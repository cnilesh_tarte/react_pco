import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { applyMiddleware, combineReducers, createStore, compose } from 'redux';
import { ToastsContainer, ToastsContainerPosition, ToastsStore } from 'react-toasts';

import * as reducers from './Reducers';

const rootReducer = combineReducers(Object.assign({}, 
  reducers,
));
const enhancer = compose(
  applyMiddleware(thunkMiddleware),
);
const store = createStore(rootReducer, enhancer);

ReactDOM.render(
  <Provider store={store}>
    <ToastsContainer position={ToastsContainerPosition.TOP_RIGHT} store={ToastsStore} />
      <App />
  </Provider>,

  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
