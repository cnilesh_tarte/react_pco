export const GET_USER_DETTAILS_REQUEST = 'GET_USER_DETTAILS_REQUEST';
export const GET_USER_DETTAILS_SUCCESS = 'GET_USER_DETTAILS_SUCCESS';
export const GET_USER_DETTAILS_FAILURE = 'GET_USER_DETTAILS_FAILURE';

export const GET_USER_DETTAILS_BY_ID_REQUEST = 'GET_USER_DETTAILS_BY_ID_REQUEST';
export const GET_USER_DETTAILS_BY_ID_SUCCESS = 'GET_USER_DETTAILS_BY_ID_SUCCESS';
export const GET_USER_DETTAILS_BY_ID_FAILURE = 'GET_USER_DETTAILS_BY_ID_FAILURE';

export const UPDATE_USER_DETTAILS_BY_ID_REQUEST = 'UPDATE_USER_DETTAILS_BY_ID_REQUEST';
export const UPDATE_USER_DETTAILS_BY_ID_SUCCESS = 'UPDATE_USER_DETTAILS_BY_ID_SUCCESS';
export const UPDATE_USER_DETTAILS_BY_ID_FAILURE = 'UPDATE_USER_DETTAILS_BY_ID_FAILURE';

